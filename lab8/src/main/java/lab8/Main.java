package lab8;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Introvert introvert = new Introvert();

        Arrays.stream(introvert.getClass().getDeclaredMethods())
                .filter(method -> method.isAnnotationPresent(RunNTimes.class))
                .forEach(method -> {
                    try {
                        method.setAccessible(true);
                        RunNTimes annotation = method.getAnnotation(RunNTimes.class);
                        int n = annotation.value();
                        for (int i = 0; i < n; i++) {
                            method.invoke(introvert);
                        }
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    }
                });
    }
}
