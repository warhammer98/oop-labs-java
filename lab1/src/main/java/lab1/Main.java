package lab1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class Main {
    public static void main(String[] args) {
        List<Integer> array = new ArrayList<>(Arrays.asList(1,2,3,4,5,6,7));

        List<Integer> shuffled = shuffleArray(array);

        first: while (true) {
            for (int i = 0; i < array.size(); i++) {
                if (array.get(i).equals(shuffled.get(i))) {
                    shuffled = shuffleArray(array);
                    continue first;
                }
            }
            break;
        }
        System.out.println(array);
        System.out.println(shuffled);
    }


    private static List<Integer> shuffleArray(List<Integer> array)
    {
        List<Integer> newArray = new ArrayList<>(array);

        Random rnd = ThreadLocalRandom.current();
        for (int i = newArray.size() - 1; i > 0; i--)
        {
            int index = rnd.nextInt(i + 1);
            // Simple swap
            int a = newArray.get(index);
            newArray.set(index,newArray.get(i));
            newArray.set(i, a);
        }

        return newArray;
    }

}
