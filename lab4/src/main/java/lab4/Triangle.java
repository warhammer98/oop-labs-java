package lab4;

import lombok.Getter;

@Getter
public class Triangle {

    private Point pa;
    private Point pb;
    private Point pc;

    private Line sa;
    private Line sb;
    private Line sc;

    public Triangle(double x1, double y1, double x2, double y2, double x3, double y3) {
        pa = new Point(x1, y1);
        pb = new Point(x2, y2);
        pc = new Point(x3, y3);
        sa = new Line(pa, pb);
        sb = new Line(pb, pc);
        sc = new Line(pc, pa);
    }

    public Point getMediansIntersectionPoint() {
        double a = sb.getLength();
        double b = sc.getLength();
        double c = sa.getLength();
        double x = (a * pa.getX() + b * pb.getX() + c * pc.getX()) / (a + b + c);
        double y = (a * pa.getY() + b * pb.getY() + c * pc.getY()) / (a + b + c);
        return new Point(x, y);
    }

    public double getPerimeter() {
        return sa.getLength() + sb.getLength() + sc.getLength();
    }

    public double getArea() {
        double p = getPerimeter() / 2;
        return Math.sqrt(p * (p - sa.getLength()) * (p - sb.getLength()) * (p - sc.getLength()));
    }

}
