package lab4;

public class Main {
    public static void main(String[] args) {
        Triangle triangle = new Triangle(0, 0, 0, 5, 5, 0);

        double area = triangle.getArea();
        double perimeter = triangle.getPerimeter();

        Point mediansIntersectionPoint = triangle.getMediansIntersectionPoint();

        return;
    }
}
