package lab3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Main {
    public static void main(String[] args) {

        String str = "belloooaorldddcjfdfcfffjusfugv";
        System.out.println(str);

        List<String> strings = new ArrayList<>();
        while (!str.isEmpty()) {
            strings.add(str.substring(0, Math.min(3, str.length())));
            str = str.substring(Math.min(3, str.length()));
        }

        strings.stream()
                .map(s -> new StringBuilder(s)
                        .replace(1, 2, randomSymbol(s)).toString())
                .sorted(String::compareTo)
                .forEach(System.out::println);
    }

    public static String randomSymbol(String str) {
        Random r = new Random();

        while (true) {
            Character randomSymbol = (char) (48 + r.nextInt(47));
            if (!str.contains(randomSymbol.toString())) {
                return randomSymbol.toString();
            }
        }
    }
}
