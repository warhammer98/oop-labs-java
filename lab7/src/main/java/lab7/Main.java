package lab7;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {

        Double average = getAverage(Arrays.asList(1,2,3,4,5,6,7));
        System.out.println("Average: " + average);

        List<String> upperCaseStrings = toUpper(Arrays.asList("Hello", "worlD"));
        System.out.println(upperCaseStrings);

        List<Integer> squares = getUniqueSquares(Arrays.asList(1,3,3,4,7,5,6,6,7));
        System.out.println(squares);

    }

    private static Double getAverage(List<Integer> list) {
        return list.stream().mapToInt(value -> value).average().orElse(Double.NaN);
    }

    private static List<String> toUpper(List<String> list) {
        return list.stream().map(String::toUpperCase).collect(Collectors.toList());
    }

    private static List<Integer> getUniqueSquares(List<Integer> list) {
        return list.stream().distinct().map(integer -> integer * integer).collect(Collectors.toList());
    }
}
