package lab5;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        while (true) {
            System.out.print("Enter Integer ('exit' to exit)");
            String input = br.readLine();
            try {
                Integer.parseInt(input);
                Deque<Object> deque = new ArrayDeque<>();
                input.chars().mapToObj(i -> (char)i)
                        .forEach(e -> deque.push(e));

                StringBuilder output = new StringBuilder();
                while (!deque.isEmpty()) output.append(deque.pop());
                System.out.println(output);
            } catch (NumberFormatException e) {
                if (input.equals("exit")) return;
                else System.out.println("Input is not a number and is not an 'exit' string");
            }
        }

    }
}
