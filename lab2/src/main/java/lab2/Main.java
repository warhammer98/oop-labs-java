package lab2;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Main {

    private static final Random random = new Random();

    public static void main(String[] args) {
        List<List<Integer>> matrix = IntStream.range(0, 5)
                .mapToObj(idx -> new ArrayList<>(IntStream.range(0, 5)
                        .boxed()
                        .map(integer -> 1000)
                        .map(random::nextInt)
                        .collect(Collectors.toList())
                ))
                .collect(Collectors.toList());


        int strToRemoveIdx = IntStream.range(0, matrix.size())
                .boxed()
                .map(idx -> Arrays.asList(idx, Collections.max(matrix.get(idx))))
                .min(Comparator.comparing(o -> o.get(1)))
                .get().get(0);

        matrix.forEach(str -> System.out.println(str + "\n"));
        System.out.println("\n---------------------------\n");
        matrix.remove(strToRemoveIdx);
        matrix.forEach(str -> System.out.println(str + "\n"));

    }
}
